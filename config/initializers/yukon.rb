paypal_config = Rails.application.config_for(:paypal)

Yukon.configuration do |config|
  config.login     = paypal_config['login']
  config.password  = paypal_config['password']
  config.signature = paypal_config['signature']
  config.mode      = paypal_config['mode']
end