# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])

Product.create(name: 'Rich Dad', price: 39.99, description: 'ABC' )
Product.create(name: 'Ubuntu Phone', price: 390.95, description: 'XYZ' )
Product.create(name: 'Orange', price: 3.99, description: 'sweet' )
Product.create(name: 'Sofa', price: 450.99, description: 'comfy' )