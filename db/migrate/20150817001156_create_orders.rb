class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :cart_id
      t.string  :ip_address

      t.text    :details
      
      t.timestamps null: false
    end
  end
end
