class Order < ActiveRecord::Base
  belongs_to :cart
  has_many :payments
  
  serialize :details
  
  def price_in_cents
    (cart.total_price*100).round
  end
      
end
